import tensorflow as tf
import tensorflow_datasets as tfds

def get_batch_data(file_list, batch_size, input_size, label_shape=None):
    dataset = tf.data.TFRecordDataset(file_list)

    sequence_features = {
        'inputs': tf.io.FixedLenSequenceFeature(shape=[input_size],
                                            dtype=tf.float32),
        'labels': tf.io.FixedLenSequenceFeature(shape=label_shape or [],
                                            dtype=tf.int64)}

    def parse(serialized):
        _, sequence = tf.io.parse_single_sequence_example(serialized, sequence_features=sequence_features)
        length = tf.shape(sequence['inputs'])[0]
        return sequence['inputs'], sequence['labels'], length

    # dataset = dataset.map(parse).batch(batch_size = 32)
    iterator = dataset.__iter__()
    return iterator


  # tf.logging.info(input_tensors)
  # return tf.train.batch(
      # input_tensors,
      # batch_size=batch_size,
      # capacity=QUEUE_CAPACITY,
      # num_threads=num_enqueuing_threads,
      # dynamic_pad=True,
      # allow_smaller_final_batch=False)


dataset = get_batch_data(["/data/maestro.tfrecord"], batch_size=32, input_size=1000)
x = dataset.next().numpy().decode('latin')
input_size = 1000
label_shape = 10
sequence_features = {
    'inputs': tf.io.FixedLenSequenceFeature(shape=[None],
                                        dtype=tf.float32, allow_missing=True),
    'labels': tf.io.FixedLenSequenceFeature(shape=label_shape or [],
                                        dtype=tf.int64)}
p = tf.io.parse_single_sequence_example(x, sequence_features)

__import__('pdb').set_trace()
